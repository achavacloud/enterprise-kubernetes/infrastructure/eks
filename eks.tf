module "eks" {
  source                          = "terraform-aws-modules/eks/aws"
  version                         = "19.15.3"
  cluster_name                    = "${var.cluster_name}-${var.env}"
  cluster_version                 = var.cluster_version
  vpc_id                          = var.vpc_id
  subnet_ids                      = var.private_subnet_ids
  cluster_endpoint_private_access = true
  cluster_endpoint_public_access  = true
  create_kms_key                  = true
  cluster_encryption_config       = {}

  cluster_enabled_log_types = ["api", "audit", "authenticator", "controllerManager", "scheduler"]
  cluster_addons = {
    coredns = {
      resolve_conflicts = "OVERWRITE"
      addon_version     = var.cluster_addon_core_dns_version
    }
    aws-ebs-csi-driver = {
      resolve_conflicts        = "OVERWRITE"
      addon_version            = var.cluster_addon_ebs_csi_version
      service_account_role_arn = module.ebs_csi_irsa_role.iam_role_arn
    }
    kube-proxy = {
      resolve_conflicts = "OVERWRITE"
      addon_version     = var.cluster_addon_kube_proxy_version
    }
    vpc-cni = {
      resolve_conflicts = "OVERWRITE"
      addon_version     = var.cluster_addon_vpc_cni_version
      service_account_role_arn = module.vpc_cni_irsa_role.iam_role_arn
    }
    aws-efs-csi-driver = {
      addon_version     = var.cluster_addon_efs_csi_version
      resolve_conflicts = "OVERWRITE"
      service_account_role_arn = module.efs_csi_irsa_role.iam_role_arn
    }
    eks-pod-identity-agent = {
      addon_version     = var.cluster_addon_pod_identity_version
      resolve_conflicts = "OVERWRITE"
    }
  }

  # aws-auth configmap
  manage_aws_auth_configmap = true
  aws_auth_roles = [
    {
      rolearn  = var.cluster_admin_arn,
      username = "adminsso"
      groups   = ["system:masters"]
    }
  ]
  aws_auth_users = [
    {
      userarn  = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/${var.cluster_admin_user_tfc}"
      username = var.cluster_admin_user_tfc
      groups   = ["system:masters"]
    },
  ]

  # Extend node-to-node security group rules
  node_security_group_additional_rules = {
    ingress_self_all = {
      description = "Node to node all ports/protocols"
      protocol    = "-1"
      from_port   = 0
      to_port     = 0
      type        = "ingress"
      self        = true
    }
    ingress_cluster_all = {
      description                   = "Cluster to node all ports/protocols"
      protocol                      = "-1"
      from_port                     = 0
      to_port                       = 0
      type                          = "ingress"
      source_cluster_security_group = true
    }
    ingress_node_exporter = {
      description = "prometheus node exporter"
      protocol    = "tcp"
      from_port   = 9100
      to_port     = 9100
      type        = "ingress"
      cidr_blocks = ["10.0.0.0/8"]
    }
    egress_all = {
      description      = "Node all egress"
      protocol         = "-1"
      from_port        = 0
      to_port          = 0
      type             = "egress"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }
  }
  eks_managed_node_group_defaults = {
    subnet_ids                            = var.private_subnet_ids
    attach_cluster_primary_security_group = true
    timeouts = {
      update = "120m"
    }
    ami_release_version = var.worker_ami_version
    iam_role_additional_policies = {
      eks_allow_manage_ebs_volumes    = "arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy"
      eks_node_group_cloud_watch_full = "arn:aws:iam::aws:policy/CloudWatchFullAccess"
    }
    block_device_mappings = {
      xvda = {
        device_name = "/dev/xvda"
        ebs = {
          volume_size           = 10
          volume_type           = "gp3"
          iops                  = 3000
          throughput            = 125
          encrypted             = true
          delete_on_termination = true
        }
      }
    }
  }
  eks_managed_node_groups = {
    cloud-infra = {
      description    = "Default EKS Managed Node Group"
      min_size       = var.worker_x86_min_capacity
      max_size       = var.worker_x86_max_capacity
      desired_size   = var.worker_x86_desired_capacity
      ami_type       = var.worker_x86_ami_type
      instance_types = var.worker_x86_instance_type
      capacity_type  = var.worker_x86_capacity_type
      labels         = var.worker_x86_labels
      taints         = var.worker_x86_taints
    }
  }
}

