################################################################################
# IRSA Roles for EKS Cluster
################################################################################
##Required for PVC attachment to work, e.g. for Prometheus pvc
module "ebs_csi_irsa_role" {
  source                = "terraform-aws-modules/iam/aws//modules/iam-role-for-service-accounts-eks"
  version               = "5.0.0"
  role_name             = join("-", [module.eks.cluster_name, "ebs-csi"])
  attach_ebs_csi_policy = true
  oidc_providers = {
    main = {
      provider_arn               = module.eks.oidc_provider_arn
      namespace_service_accounts = ["default:default", "kube-system:ebs-csi-controller-sa"]
    }
  }
}

resource "aws_iam_role_policy_attachment" "ebs_csi_controller_attachment" {
  role       = module.ebs_csi_irsa_role.iam_role_name
  policy_arn = aws_iam_policy.ebs_csi_controller_policy.arn
}

resource "aws_iam_policy" "ebs_csi_controller_policy" {
  name        = "${module.eks.cluster_name}-ebs-csi-controller-policy"
  description = "Additional permissions for EBS CSI Controller"
  policy      = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect   = "Allow",
        Action   = [
          "ec2:CreateVolume",
          "ec2:DeleteVolume",
          "ec2:AttachVolume",
          "ec2:DetachVolume",
          "ec2:DescribeVolumes",
          "ec2:DescribeVolumeAttribute",
          "ec2:DescribeVolumeStatus",
          "ec2:DescribeInstanceStatus"
        ],
        Resource = "*"
      }
    ]
  })
}

module "vpc_cni_irsa_role" {
  source                = "terraform-aws-modules/iam/aws//modules/iam-role-for-service-accounts-eks"
  version               = "5.0.0"
  role_name             = join("-", [module.eks.cluster_name, "vpc-cni"])
  attach_vpc_cni_policy = true
  vpc_cni_enable_ipv4   = true
  oidc_providers = {
    main = {
      provider_arn               = module.eks.oidc_provider_arn
      namespace_service_accounts = ["default:default", "kube-system:aws-node"]
    }
  }
}

module "efs_csi_irsa_role" {
  source                = "terraform-aws-modules/iam/aws//modules/iam-role-for-service-accounts-eks"
  version               = "5.0.0"
  role_name             = join("-", [module.eks.cluster_name, "efs-csi-irsa"])
  attach_efs_csi_policy = true
  oidc_providers = {
    main = {
      provider_arn               = module.eks.oidc_provider_arn
      namespace_service_accounts = ["kube-system:efs-csi-controller-sa"]
    }
  }
}
################################################################################
# IRSA Roles for Karpenter
################################################################################
module "karpenter_irsa_role" {
  source                             = "terraform-aws-modules/iam/aws//modules/iam-role-for-service-accounts-eks"
  version                            = "5.24.0"
  role_name                          = "karpenter-controller-${module.eks.cluster_name}"
  attach_karpenter_controller_policy = true

  karpenter_controller_cluster_name       = module.eks.cluster_name
  karpenter_controller_node_iam_role_arns = [module.eks.eks_managed_node_groups["cloud-infra"].iam_role_arn]
  oidc_providers = {
    main = {
      provider_arn               = module.eks.oidc_provider_arn
      namespace_service_accounts = ["karpenter:karpenter"]
    }
  }
}
#aws_iam_instance_profile resource in this Terraform configuration is used to associate an IAM role with the EC2 instances that are launched by Karpenter.
#aws_iam_instance_profile resource ensures that the EC2 instances provisioned by Karpenter have the necessary permissions to operate correctly and securely within the AWS environment and the EKS cluster. This is a critical aspect of managing permissions and security in a cloud-native infrastructure.
resource "aws_iam_instance_profile" "karpenter" {
  name = "${module.eks.cluster_name}-karpenter-node-instance-profile"
  role = module.eks.eks_managed_node_groups["cloud-infra"].iam_role_name
}