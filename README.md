## AWS EKS Cluster Terraform Module

#### This Terraform module sets up an Amazon Elastic Kubernetes Service (EKS) cluster with a managed node group. It includes configurations for core EKS features, additional add-ons, and security policies, all defined through modular and reusable infrastructure as code.

Features

- EKS Cluster: Creates an EKS cluster with specified configurations, including control over cluster access endpoints and logging.
- Managed Node Groups: Defines EKS managed node groups with customizable instance types, sizes, and policies.
- Add-ons: Supports deploying critical add-ons such as CoreDNS, VPC CNI, EBS CSI Driver, EFS CSI Driver, and EKS Pod Identity Agent.
- IAM Roles and Policies: Manages IAM roles for add-ons and assigns necessary permissions.
- Network and Security: Configures security groups, node-to-node communication rules, and VPC settings for secure and efficient networking.

#### Module Structure

The module is structured to allow users to specify various parameters to configure the EKS cluster and its components, making it flexible and adaptable to different environments and requirements.
