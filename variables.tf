# Values for resource tagging
variable "app" {
  description = "Application Name"
  default     = ""
}

variable "cluster_name" {
  description = "Cluster Name"
}
variable "department" {
  description = "Department tag"
  default     = ""
}

variable "project_name" {
  description = "Project Name Tag"
  default     = ""
}

variable "project_code" {
  description = "Project Code Tag"
  default     = ""
}

variable "cost_center" {
  description = "Cost Center Tag"
  default     = ""
}

variable "security_data_classification" {
  description = "security data classification"
  default     = "internal"
}

variable "node_exporter" {
  description = "node exporter enabled or disabled"
  default     = ""
}

variable "monitoring_owner" {
  description = "owner name for monitoring via prometheus"
  default     = ""
}

#EKS Variables
variable "env" {
  type        = string
  description = "Environment We're building for"
}

variable "region" {
  type        = string
  description = "AWS region we are deploying our resources"
}

variable "cluster_version" {
  description = "EKS Cluster Version"
  type = string
}

variable "cluster_addon_core_dns_version" {
  description = "EKS Addon Version"
}

variable "cluster_addon_kube_proxy_version" {
  description = "EKS Addon Version"
}

variable "cluster_addon_ebs_csi_version" {
  description = "EKS Addon Version"
}

variable "cluster_addon_efs_csi_version" {
  description = "EKS Addon Version"
}

variable "cluster_addon_vpc_cni_version" {
  description = "EKS Addon Version"
}

variable "cluster_addon_pod_identity_version" {
  description = "EKS Addon Version"
}

variable "vpc_id" {
  type        = string
  description = "The vpc id used for EKS"
}

variable "private_subnet_ids" {
  description = "A list of subnet IDs where the EKS cluster (ENIs) will be provisioned along with the nodes/node groups."
}

variable "cluster_admin_arn" {
  description = "IAM Role ARN of which AWS role can manage the cluster. See default for example"
  default     = ""
}

variable "cluster_admin_user_tfc" {
  type        = string
  description = "IAM user that is added to aws_auth. Used by Terraform to make changes to k8s cluster resources"
}

variable "worker_ami_version" {
  type        = string
  description = "EKS worker AMI version"
  default     = "1.25.12-20230825" # https://github.com/awslabs/amazon-eks-ami/releases
}

# x86 node group variables
variable "worker_x86_ami_type" {
  type        = string
  description = "EKS worker AMI type"
  default     = "AL2_x86_64"
}

variable "worker_x86_desired_capacity" {
  type        = number
  description = "EKS worker ASG desired capacity"
  default     = 0
}

variable "worker_x86_max_capacity" {
  type        = number
  description = "EKS worker ASG maximum capacity"
  default     = 0
}

variable "worker_x86_min_capacity" {
  type        = number
  description = "EKS worker ASG minimum capacity"
  default     = 0
}

variable "worker_x86_instance_type" {
  description = "EKS worker instance type"
  default     = []
}

variable "worker_x86_capacity_type" {
  description = "EKS worker capacity type"
  default     = "SPOT"
}

variable "worker_x86_labels" {
  type        = map(string)
  description = "List of labels for node group"
  default     = {}
}

variable "worker_x86_taints" {
  type = list(object({
    key    = string
    value  = string
    effect = string
  }))
  description = "List of taints for node group"
  default     = []
}

#TFE Vars
variable "tfc_workspace" {
  description = "TFE variable"
  default     = ""
}

variable "aws_secret_key" {
  description = "TFE variable"
  default     = ""
}

variable "aws_access_key" {
  description = "TFE variable"
  default     = ""
}
